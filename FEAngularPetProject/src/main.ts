import { bootstrapApplication } from '@angular/platform-browser';
import { appConfig } from './app/app.config';
import { AppComponent } from './app/app.component';
import { Route } from '@angular/router';


export const ROUTES: Route[] = [
  {path: 'home', loadChildren: () => import('./app/app.routes').then(mod => mod.routes)},
  
]

bootstrapApplication(AppComponent, appConfig)
  .catch((err) => console.error(err));
