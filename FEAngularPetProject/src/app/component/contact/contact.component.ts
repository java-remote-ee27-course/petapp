import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpResponse } from '@angular/common/http';
import { FormsModule, NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { IContactForm } from '../../../../models/IContactForm';
import { PictureService } from '../../services/picture.service';
import { Observable } from 'rxjs';



@Component({
    selector: 'app-contact',
    standalone: true,
    templateUrl: './contact.component.html',
    styleUrl: './contact.component.css',
    imports: [
        HttpClientModule,
        CommonModule,
        FormsModule
    ]
})
export class ContactComponent implements OnInit {
    contact: IContactForm = {
        id: 0,
        name: '',
        email: '',
        topic: '',
        message: ''
    };
 

    currentFile?: File;
    msg: string = '';
    imageInfos?: Observable<any>;
    preview: any = '';

    constructor() { }

    onSubmit(contactForm: NgForm) {

    }
     
    ngOnInit(): void {

        
    }

}
