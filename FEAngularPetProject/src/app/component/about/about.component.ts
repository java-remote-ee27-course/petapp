import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatcardComponent } from "../matcard/matcard.component";
import { IMatCardPicture } from '../../../../models/IMatCardPicture';
import { MATCARDPICTURES } from '../../card-content';

@Component({
    selector: 'app-about',
    standalone: true,
    templateUrl: './about.component.html',
    styleUrl: './about.component.css',
    imports: [CommonModule, FormsModule, MatCardModule, MatButtonModule, MatcardComponent]
})
export class AboutComponent {
  matcards : IMatCardPicture[] = MATCARDPICTURES;
  spacer : string = "separator";
  matTitle : string = "mat-title";


}
