import { Component } from '@angular/core';
import { CarouselComponent } from "../carousel/carousel.component";
import { HomeMainComponent } from "../home-main/home-main.component";



@Component({
    selector: 'app-home',
    standalone: true,
    templateUrl: './home.component.html',
    styleUrl: './home.component.css',
    imports: [CarouselComponent, HomeMainComponent]
})
export class HomeComponent {


}
