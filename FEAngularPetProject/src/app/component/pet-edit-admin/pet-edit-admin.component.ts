import { FormsModule, NgForm } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Component, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PetService } from '../../services/pet.service';
import { Location } from '@angular/common';
import { IPet } from '../../../../models/IPet';
import { UserService } from '../../services/user.service';
import { IUser } from '../../../../models/IUser';
import { IPetWithUser } from '../../../../models/IPetWithUser';

@Component({
  selector: 'app-pet-edit-admin',
  standalone: true,
  imports: [FormsModule, CommonModule],
  templateUrl: './pet-edit-admin.component.html',
  styleUrl: './pet-edit-admin.component.css'
})
export class PetEditAdminComponent {
  @ViewChild('petEditAdminForm') private petEditAdminForm!: NgForm;
  usersAdminEditUrl: string = 'admins/users';
  petsAdminEditUrl: string = 'admins/pets';
  petWithUser!: IPetWithUser;
  errorMessage: string = '';
  users: Array<IUser> = [];
  pet!: IPet ;
  selectedUser: IUser = {
    id: 0,
    username: '',
  };
  messageEmitted:string ='';

  // TODO:: replace IPet with IPetWithUser 

  constructor(
    private route: ActivatedRoute,
    private location: Location,
    private petService: PetService,
    private userService: UserService

  ) { }

  ngOnInit(): void {
    this.getPet();
    this.getUsers();
  }

  getPet(): void {
    // Obtaining the value of "id" parameter and converting to number
    // +this... (unary plus -- trying to convert value to number)
    const id = +this.route.snapshot.paramMap.get('id')!;

    //get pet info:
    this.petService.getPet(this.petsAdminEditUrl, id)
      .subscribe(pet => this.pet = pet);
  }

  //Navigates back in the browser URL history.
  goBack(): void {
    this.location.back(); 
  }


  //save and go back to previous page
  onEdit(): void {
    if (this.petEditAdminForm.valid) {
      if(this.getOwnerForPet() != null){
         this.pet.user = this.getOwnerForPet();
      } 
      this.petService.updatePet(this.petsAdminEditUrl, this.pet)
        .subscribe(
          {
            next: () => this.goBack(),
            error: (err) => {
              this.errorMessage = "Something went wrong, pet not changed!";
            }
          }
        );
    }
  }

  // gets users by UserService for the "Pet edit" form
  getUsers(): IUser[] {
    this.userService.getUsers(this.usersAdminEditUrl).subscribe(
      value => {
        this.users = value;
      },
    );
    return this.users;
  }

  getOwnerForPet(): any {
    // object retrieved because of <select ... [(ngModel)]="selectedUser"> and 
    // <option [ngValue]="thisSelectedUser" *ngFor="let thisSelectedUser of users">
    return this.selectedUser;
  }
}
