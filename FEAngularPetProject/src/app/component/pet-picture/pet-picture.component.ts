import { CommonModule } from '@angular/common';
import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { PictureService } from '../../services/picture.service';
import { HttpResponse } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { IFile } from '../../../../models/IFile';

/** This class is to be deleted if not needed. Right now it is not used anywhere. */

@Component({
  selector: 'app-pet-picture',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './pet-picture.component.html',
  styleUrl: './pet-picture.component.css'
})
export class PetPictureComponent implements OnInit {

  msg: string = '';
  preview: any = '';
  currentFile?: File;
  isPictureAdded!: boolean;
  imageInfos?: Observable<any>;
  imageInfo?: IFile = { 
    id:0, 
    imgName: '', 
    url:'',
    type:'',
    imgData:'',
    date:''
  }; //Observable<any>; 
  pictureName: string | undefined;

  constructor(private pictureService: PictureService) {

  }

  ngOnInit() {
    this.pictureService.getLatestFileByDate();

  }

  selectFile(event: any): void {
    this.isPictureAdded = false;
    this.msg = '';
    this.preview = '';
    const selectedFiles = event.target.files;
    if (selectedFiles) {
      const file: File | null = selectedFiles.item(0);

      if (file) {

        this.currentFile = file;
        const reader = new FileReader();

        reader.onload = (img: any) => {
          console.log(img.target.result)
        };
        reader.readAsDataURL(this.currentFile);

          this.upload();
      }
    }
  }


  //This is working
  previewFile(): void{
    const reader = new FileReader();

    reader.onload = (img: any) => {
        
        this.preview = img.target.result;
        console.log("IMG: " + this.preview)
    };
    reader.readAsDataURL(this.currentFile!);
  }

  //Idea received from: https://www.bezkoder.com/angular-17-file-upload/
  upload(): void {
    
    if (this.currentFile) {
      this.pictureService.upload(this.currentFile).subscribe({ 
        next: (event: any) => {
          if (event instanceof HttpResponse) {
            this.msg = event.body.message;

            console.log("Current file " + this.currentFile?.type);

            //check if picture is shown
            this.isPictureAdded = !this.isPictureAdded;
            console.log("Image is: " + this.isPictureAdded);
            
            //Get the file that was last uploaded:
            this.pictureService.getLatestFileByDate().subscribe({
              next: 
                (img) => {this.imageInfo = img;
              },
              error: (err) => {console.error('Error fetching image:', err)}
            });

          }
        },
        error: (err: any) => {
          console.log(err);

          if (err.error && err.error.message) {
            this.msg = err.error.message;
          } else {
            this.msg = 'Could not upload the file!';
          }
        },
        complete: () => {
          this.currentFile = undefined;
        },
      });
    }
  }
}


