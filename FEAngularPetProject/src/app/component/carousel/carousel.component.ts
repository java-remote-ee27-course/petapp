import { CommonModule } from '@angular/common';
import { Component, Input } from '@angular/core';
import { IPicture } from '../../../../models/IPicture';
import { IHeadings } from '../../../../models/IHeadings';
import { ICarouselOptions } from '../../../../models/ICarouselOptions';



/** Re-usable carousel component */

@Component({
  selector: 'app-carousel',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './carousel.component.html',
  styleUrl: './carousel.component.css'
})



export class CarouselComponent {

  @Input() showControls: boolean = true;

  // create an array, type of IPicture and
  // get the input from parent component
  // e.g, home-main
  @Input() iPictures: IPicture[] = [];

  @Input() headText: IHeadings | undefined;

  @Input() bootstrapButtonNext: ICarouselOptions | undefined;


}
