import { MatCardAppearance, MatCardModule } from '@angular/material/card';
import { Component, Input } from '@angular/core';
import {MatButtonModule} from '@angular/material/button';

import { CarouselComponent } from "../carousel/carousel.component";
import { IMatCardPicture } from '../../../../models/IMatCardPicture';
@Component({
    selector: 'app-matcard',
    standalone: true,
    templateUrl: './matcard.component.html',
    styleUrl: './matcard.component.css',
    imports: [MatCardModule, CarouselComponent, MatButtonModule]
})
export class MatcardComponent {
    @Input() appearance: MatCardAppearance | undefined
    @Input() iMatCardPicture: IMatCardPicture = {
        id: 0,
        title:'',
        path:'',
        description:'',
        author:'',
        source:'',
        alt:''
      }
    @Input() matCardSeparatorCssClass: string ='';
    @Input() matCardTitleCssClass:string =''; 
  
}
