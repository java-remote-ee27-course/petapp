import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PetClinicsComponent } from './pet-clinics.component';

describe('PetClinicsComponent', () => {
  let component: PetClinicsComponent;
  let fixture: ComponentFixture<PetClinicsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [PetClinicsComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(PetClinicsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
