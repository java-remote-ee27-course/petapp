import { Component, OnInit, ViewChild } from '@angular/core';
import { PetService } from '../../services/pet.service';
import { CommonModule } from '@angular/common';
import { HttpClientModule, HttpResponse } from '@angular/common/http';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { IPet } from '../../../../models/IPet';
import { FormsModule, NgForm } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PictureService } from '../../services/picture.service';
import { PetPictureComponent } from "../pet-picture/pet-picture.component";
import { IFile } from '../../../../models/IFile';
import { Observable } from 'rxjs';



@Component({
  selector: 'app-pets',
  standalone: true,
  templateUrl: './pets.component.html',
  styleUrl: './pets.component.css',
  imports: [
    CommonModule,
    RouterModule,
    HttpClientModule,
    MatTableModule,
    MatButtonModule,
    FormsModule,
    PetPictureComponent
  ]
})
export class PetsComponent implements OnInit {
  // pass this url ending to pet.service when subscring
  // in order to use pet.service from multiple components
  // which need to access different urls:
  petsUrl: string = 'pets';
  errorMessage: string = '';
  pets: Array<IPet> = [];

  //PetPicture:
  msg: string = '';
  preview: any = '';
  currentFile?: File;
  idString: string = '';

  imageInfo?: IFile = {
    id: 0,
    imgName: '',
    url: '',
    type: '',
    imgData: '',
    date: ''
  };
  pictureName: string | undefined;

  // to access / inject form in our class. Using @ViewChild 
  @ViewChild('petForm') private petForm!: NgForm;

  constructor(private petService: PetService, private pictureService: PictureService) { }

  ngOnInit(): void {
    this.getPets();
  }

  getPets(): void {
    this.petService.getPets(this.petsUrl).subscribe(value => {
      this.pets = value;
      //console.log("THIS.PETS" + value);
      //this.pets.forEach((pet) => console.log("PET TYPE: " + pet.petType + " PICTURE " + pet.picture?.url + "size " + pet.picture?.size));
    },
    );
  }

  onDelete(pet: IPet): void {
    this.petService.deletePet(this.petsUrl, pet).subscribe({
      next: () => this.pets = this.pets.filter(s => s !== pet),
      error: (err) => {
        console.log(err);
        //this.errorMessage = "You are not authorized to delete pets data!";
      }
    });
  }

  onAdd(name: string, petType: string) {
    name = name.trim();
    petType = petType.trim();
    if (this.petForm.valid && this.currentFile === undefined) {
      this.makePetServiceCall(name, petType, 0);
    } else if (this.petForm.valid) {
      this.upload().subscribe({
        next: (imageId: string) => {
          let imageIdNum: number = parseInt(imageId);
          this.makePetServiceCall(name, petType, imageIdNum);
        }
      });
    } else {
      console.log("Error in adding data to table!");
    }
  }

  //Calling this onAdd():
  makePetServiceCall(name: string, petType: string, imageIdNum: number) {
    this.petService.addPet(this.petsUrl, { name, petType } as IPet, imageIdNum)
      .subscribe({
        next: () => {
          this.petService.getPets(this.petsUrl).subscribe({
            next: (result: IPet[]) => this.pets = result,
            error: (err) => console.log(err),
          });
        },
        error: (err) => {
          console.log(err);
          //this.errorMessage = "You are not authorized to add pets data to the table!";
        }
      })
  }

  selectFile(event: any): void {
    this.msg = '';
    this.preview = '';
    const selectedFiles = event.target.files;
    if (selectedFiles) {
      const file: File | null = selectedFiles.item(0);
      if (file) {
        this.preview = '';
        this.currentFile = file;
        const reader = new FileReader();

        reader.onload = (img: any) => {
          //console.log(img.target.result)
          this.previewFile();
        };
        reader.readAsDataURL(this.currentFile);
      }
    }
  }

  //Idea received from: https://www.bezkoder.com/angular-17-file-upload/
  upload(): Observable<string> {
    return new Observable<string>((observer) => {
      if (this.currentFile) {
        this.pictureService.upload(this.currentFile).subscribe({
          next: (event: any) => {
            if (event instanceof HttpResponse) {
              this.msg = event.body.message;
              this.idString = event.body.parameterString;
              observer.next(this.idString);
              observer.complete();
              this.preview ='';
            }
          },
          error: (err: any) => {
            console.log(err);
            observer.error(err);

            if (err.error && err.error.message) {
              this.msg = err.error.message;
            } else {
              this.msg = 'Could not upload the file!';
            }
          },
          complete: () => {
            this.currentFile = undefined;

          },
        });
      } else {
        this.currentFile = undefined;
        observer.error("no upload file selected!");
        observer.next(this.idString);
      }
    })
  }


  previewFile(): void {
    const reader = new FileReader();
    reader.onload = (img: any) => {
      this.preview = img.target.result;
      //console.log("IMG: " + this.preview);
    };
    reader.readAsDataURL(this.currentFile!);
  }
}