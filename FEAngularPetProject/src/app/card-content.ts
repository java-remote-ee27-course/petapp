import { IPicture } from "../../models/IPicture";
import { IHeadings } from "../../models/IHeadings";
import { ICarouselOptions } from "../../models/ICarouselOptions";
import { IMatCardPicture } from "../../models/IMatCardPicture";

let description1: string = ``;

let description2: string = ``;

let description3: string = ``;

let matCardDescription1: string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

let matCardDescription2: string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

let matCardDescription3: string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";

export const CARDPICTURETHIRD: IPicture[] = [
    {
        id: 1,
        title: 'Little rabbit',
        path: 'assets/rabbit1.jpg',
        description: description3,
        author: 'ali atakan acikbas',
        source: '(Image from Pexels.)',
        alt: 'Little rabbit',
        class: 'carousel-item active'
    },
];

export const CARDPICTUREFIRST: IPicture[] = [
    {
        id: 1,
        title: 'Baby Cat',
        path: 'assets/cat1.jpg',
        description: description1,
        author: 'emrah ayvali',
        source: '(Image from Pexels.)',
        alt: 'Baby cat',
        class: 'carousel-item active'
    },
    {
        id: 2,
        title: 'Girl with Dog',
        path: 'assets/dog1.jpg',
        description: description2,
        author: 'julia volk',
        source: '(Image from Pexels.)',
        alt: 'Girl with Dog',
        class: 'carousel-item'
    },
    {
        id: 3,
        title: 'Reading Dog',
        path: 'assets/dog2.jpg',
        description: description3,
        author: 'samson katt',
        source: '(Image from Pexels.)',
        alt: 'Reading Dog',
        class: 'carousel-item'
     }
];
export const CARDPICTURESECOND: IPicture[] = [

    {
        id: 3,
        title: 'Girl with horse',
        path: 'assets/horse1.jpg',
        description: description2,
        author: 'jennifer murray',
        source: '(Image from Pexels.)',
        alt: 'Girl with horse',
        class: 'carousel-item active'
    }

];



export const CARDHEADING: IHeadings[] = [
    {
        id: 1,
        text: 'The Diary of My Pet'
    },
    {
        id: 2,
        text: 'Find My New Best Friend'
    },
    {
        id: 3,
        text: 'Lost and Found'
    }

];

export const CAROUSELOPTIONS: ICarouselOptions[] = [
    {
        id: 1,
        class: 'carousel-control-next',
        dataBsSlide: 'next'
    },
    {
        id: 2,
        class: 'carousel-control-prev',
        dataBsSlide: 'next'
    },

];

export const MATCARDPICTURES: IMatCardPicture [] = [
    {
        id: 1,
        title: 'Chief Happiness Officer',
        path: 'assets/cho.jpg',
        description: matCardDescription1,
        author: 'josh sorenson',
        source: '(Image from Pexels.)',
        alt: 'Chief Happiness Officer',
        class: 'example-card'
    },
    {
        id: 2,
        title: 'Health Care Officer',
        path: 'assets/hco.jpg',
        description: matCardDescription2,
        author: 'Pixabay',
        source: '(Image from Pexels.)',
        alt: 'Health Care Officer',
        class: 'example-card'
    },
    {
        id: 3,
        title: 'Head of Nutrition Department',
        path: 'assets/hnd.jpg',
        description: matCardDescription3,
        author: 'Pixabay',
        source: '(Image from Pexels.)',
        alt: 'Head of Nutrition Department',
        class: 'example-card'
    },
]
