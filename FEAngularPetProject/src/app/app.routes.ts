import { Routes } from '@angular/router';
import { HomeComponent } from './component/home/home.component';
import { AboutComponent } from './component/about/about.component';
import { ContactComponent } from './component/contact/contact.component';
import { LoginComponent } from './component/login/login.component';
import { PetsComponent } from './component/pets/pets.component';
import { authGuard } from './guards/auth.guard';
import { RegistrationComponent } from './component/registration/registration.component';
import { PetEditComponent } from './component/pet-edit/pet-edit.component';
import { PetAdminComponent } from './component/pet-admin/pet-admin.component';
import { AdminComponent } from './component/admin/admin.component';
import { adminGuard } from './guards/admin.guard';
import { ForbiddenComponent } from './component/forbidden/forbidden.component';
import { PetEditAdminComponent } from './component/pet-edit-admin/pet-edit-admin.component';
import { PetClinicComponent } from './component/pet-clinic/pet-clinic.component';
import { PetClinicsComponent } from './component/pet-clinics/pet-clinics.component';


export const routes: Routes = [
    { path: 'login', component: LoginComponent},
    { path: 'home', component: HomeComponent },
    { path: 'about', component: AboutComponent },
    { path: 'contact', component: ContactComponent }, 
    { path: 'forbidden', component: ForbiddenComponent, canActivate: [authGuard]}, 
    { path: 'pets', component: PetsComponent, canActivate: [authGuard] },
    { path: 'edit/:id', component: PetEditComponent, canActivate: [authGuard] },
    { path: 'admin', component: AdminComponent, canActivate: [adminGuard], children:[
        {path: 'petadmin', component: PetAdminComponent, canActivate: [adminGuard]},
          
    ]},
    { path: 'adminedit/:id', component: PetEditAdminComponent, canActivate: [adminGuard]},  
    { path: 'petclinics', component: PetClinicsComponent },
    { path: 'info/:id', component: PetClinicComponent }, 
    { path: 'register', component: RegistrationComponent },
    { path: '**', redirectTo: 'home', pathMatch: 'full' },
];

