import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { BrowserStorageService } from './storage.service';
import { Observable } from 'rxjs';
import { IPetClinic } from '../../../models/IPetClinic';

@Injectable({
  providedIn: 'root'
})
export class PetClinicService {
  url = `${environment.url}`;

  constructor(private httpClient : HttpClient, private storage: BrowserStorageService) { }

  getPetClinics(urlEnd: string): Observable<IPetClinic[]>{
    const url = `${this.url}/${urlEnd}`;
    return this.httpClient.get<IPetClinic[]>(url);  
  }

  getPetClinic(urlEnd:string, id:number): Observable<IPetClinic>{
    const url = `${this.url}/${urlEnd}/${id}`;
    return this.httpClient.get<IPetClinic>(url);
  }

}
