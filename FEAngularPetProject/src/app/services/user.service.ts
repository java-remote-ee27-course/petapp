import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { BrowserStorageService } from './storage.service';
import { Observable } from 'rxjs';
import { IUser } from '../../../models/IUser';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  url = `${environment.url}`;

  constructor(private httpClient: HttpClient, private auth: AuthService, private storage: BrowserStorageService) { }

  getUsers(urlEnd:string): Observable<IUser[]>  {
    const url = `${this.url}/${urlEnd}`;
    return this.httpClient.get<IUser[]>(url); 
  }

  getUser(urlEnd:string, username?: string): Observable<IUser> {
    const url = `${this.url}/${urlEnd}/${username}`;
    return this.httpClient.get<IUser>(url);
  }
}
