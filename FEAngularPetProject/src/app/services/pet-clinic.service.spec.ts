import { TestBed } from '@angular/core/testing';

import { PetClinicService } from './pet-clinic.service';

describe('PetClinicService', () => {
  let service: PetClinicService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PetClinicService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
