import { Injectable } from '@angular/core';
import { BrowserStorageService } from './storage.service';

/** 
 * Browser storage to access the browser storage when needed.
 * See the complete logic from 
 * https://javascript.plainenglish.io/how-to-handle-browser-storage-in-angular-ssr-d24391698d21 
 * See also app.config.server.ts
 */


@Injectable({
  providedIn: 'root'
})
@Injectable()
export class BrowserStorageServerService extends BrowserStorageService {
  constructor() {
    super({
      clear: () => {},
      getItem: (key: string) => JSON.stringify({ key }),
      setItem: (key: string, value: string) => JSON.stringify({ [key]: value }),
      key: (index: number) => index.toString(),
      length: 0,
      removeItem: (key: string) => JSON.stringify({ key }),
    });
  }
}