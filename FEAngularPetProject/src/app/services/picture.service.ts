import { HttpClient, HttpEvent, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, map, tap } from 'rxjs';
import moment from 'moment';
import { IFile } from '../../../models/IFile';


/**
 * Picture service, the idea from:
 * https://www.bezkoder.com/angular-17-file-upload/ and
 * https://www.javainuse.com/fullstack/imageupload
 */

@Injectable({ providedIn: 'root' })
export class PictureService {
  url = `http://localhost:8080/files`;
  message: string = '';
  dateStr: string = '';
  currentFile: any;

  constructor(private httpClient: HttpClient) { } 
  
  upload(file: File): Observable<HttpEvent<any>> {
    const formData: FormData = new FormData();
    const dateString = moment().format("YYYY-MM-DDTHH:mm:ss");

    formData.append('file', file);
    formData.append('localDateTime', dateString);

    const req = new HttpRequest('POST', `${this.url}/upload`, formData, {
      responseType: 'json',
    });

    return this.httpClient.request(req)
      .pipe(
        tap({next:() => this.dateStr = dateString, 
             error: () => console.error()})
            
      );
  }


  getFiles(): Observable<any> {
    return this.httpClient.get(`${this.url}/images`);
  }


  getLatestFileByDate(): Observable<any> {
    console.log(`${this.url}/images/image?date=${this.dateStr}`);
    return this.httpClient.get(`${this.url}/images/image?date=${this.dateStr}`);

  }
}
