import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BrowserStorageService } from './storage.service';
import { environment } from '../../environments/environment';
import { BehaviorSubject, Observable, catchError, map, throwError } from 'rxjs';
import { IUser } from '../../../models/IUser';



/**
 * AuthService checks the user provided username + password and 
 * compares them to username + password from backend (REST API get).
 * If username and password match to the credentials from BE, login and save credentials (see headers in HttpOptions)
 * If username and password do not match (or some server error), do not login (error part)
 * 
 * Logout deletes credentials from localStorage.
 * See environments/environment for URL.
 * 
 * BrowserStorageService logic can be found in: 
 * https://javascript.plainenglish.io/how-to-handle-browser-storage-in-angular-ssr-d24391698d21
 * To add the roles / user different logic:
 * https://jasonwatmore.com/post/2022/12/22/angular-14-role-based-authorization-tutorial-with-example#authentication-service-ts
 */

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  //credentials: string = '';
  //isAdministrator: boolean | undefined;
  isAuthenticated: boolean | undefined;
  petsUrl = `${environment.url}`;
  user: IUser | undefined;
  token: string | null;
  roles: string | null;
  message: string = '';
 
  constructor(
    private router: Router,
    private http: HttpClient,
    private route: ActivatedRoute,
    private storage: BrowserStorageService
  ) {
      this.token = storage.get('token');
      this.roles = storage.get('roles');
      const loggedIn = storage.get('is_authenticated');
      this.isAuthenticated = loggedIn ? JSON.parse(loggedIn) : false;
  }

  getCredentials(): string | null{
    return this.token;
  }

  getRoles(): string | null {
    return this.roles;
  }

  login(username: string, password: string) {
    //token is retrieved from value.jwt
    return this.http.post<any>(this.petsUrl + "/auth/login", {username, password})
      .pipe(
        map((value) => {
          this.user = value.user;       
          this.token = value.jwt;
          this.roles = JSON.stringify(value.user.authorities);

          if (this.token===null) {
            console.error('Error: Access token not received');
            this.message = 'Could not login!';
            return;
          } else if (this.token===undefined){
            console.error('Error: Access token is undefined'); 
            this.message = 'Could not login!';
            return; 
          } else if (this.token === ''){
            const errorMessage = 'Error: Please check username and password!';
            console.error(errorMessage);
            this.message = errorMessage;
            return;
          } 

          this.isAuthenticated = true;
          this.storage.set('token', this.token);
          this.storage.set('is_authenticated', 'true');
          this.router.navigateByUrl(this.route.snapshot.queryParams[`returnUrl`]);
          this.storage.set('roles', this.roles);
        }),
        
        catchError((error) => {
          console.log
          (
            `Could not log in. Please check 
            the connection to the server.`
            + error.message
          );
          return throwError(() => error);
        })
      );
  }

  logout(): void {
    this.storage.remove('roles');
    this.storage.remove('token');
    this.storage.remove('is_authenticated');
    this.token = '';
    this.roles = '';
    this.isAuthenticated = false;
    console.clear();
    this.router.navigate(['/login']);
  }

  register(username: string, password: string): Observable<any> {
    return this.http.post<any>(environment.url + '/auth/register', 
    { 
      username: username, 
      password: password 
    });
  }

  isAdmin():boolean {
    if(this.roles?.includes('ADMIN')) {
 //     this.isAdministrator = true;
      return true;
    }
    else {
//      this.isAdministrator = false;
      return false;
    }
  }
}
