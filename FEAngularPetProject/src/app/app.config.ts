import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import { provideClientHydration } from '@angular/platform-browser';
import { provideAnimations } from '@angular/platform-browser/animations';
import {  HttpClientModule, provideHttpClient, withFetch } from '@angular/common/http';
import { BrowserStorageService } from './services/storage.service';
import { httpInterceptorProviders } from './interceptors';


export const appConfig: ApplicationConfig = {
  providers: [
    importProvidersFrom(HttpClientModule),
    httpInterceptorProviders,
    provideHttpClient((withFetch())),
    provideRouter(routes), 
    provideClientHydration(), 
    provideAnimations(),
    BrowserStorageService,
  ]
};

