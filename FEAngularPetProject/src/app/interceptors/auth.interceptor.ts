import { HttpEvent, HttpHandler, HttpInterceptor, HttpInterceptorFn, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { BrowserStorageService } from '../services/storage.service';

/** 
 * Interceptor which intercepts header requests 
 * in order to the headers definitions (sent to BE) would not be repeated in diff. places
 * environment - see angular.json "fileReplacements" for environment
 * and environments/environment.ts & environments/environment.development.ts
 * See: https://angular.io/guide/http-intercept-requests-and-responses 
 */ 

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authService: AuthService, private storage: BrowserStorageService) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    const isApiUrl = request.url.startsWith(environment.url);
    if (isApiUrl) {
      const headers: any = {
        'Content-Type': 'application/json',
        'X-Requested-With': 'XMLHttpRequest',
      }

      //next call gets the tokens from localStorage 
      const token = this.storage.get('token');
      if (token) {
        headers.Authorization = 'Bearer ' + token;
      }

      request = request.clone({
        setHeaders: headers,
      });
    }
    return next.handle(request);
  }
}


