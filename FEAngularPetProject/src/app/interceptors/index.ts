import { HTTP_INTERCEPTORS } from "@angular/common/http";
import { AuthInterceptor } from "./auth.interceptor";
import { UploadInterceptor } from "./upload.interceptor";

/** Array of Http interceptor providers in "outside-in" order */
/** See: https://angular.io/guide/http-intercept-requests-and-responses */
export const httpInterceptorProviders = [

    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: UploadInterceptor, multi: true },
];