import { IFile } from "./IFile";
import { IUser } from "./IUser";

export interface IPet{
    id?:number;
    name:string;
    petType:string;
    username?:string;   //needed to get pet username in petadmin
    user?:IUser;
    picture?:IFile;
}