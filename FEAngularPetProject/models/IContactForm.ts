export interface IContactForm{
    id?: number;
    name: string,
    email: string,
    topic : string,
    message : string
}