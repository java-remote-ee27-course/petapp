import { IAddress } from "./IAddress";
import { IPhone } from "./IPhone";

export interface IPetClinic{
    id?:number;
    petClinicName:string;
    petClinicAddresses?:IAddress[];
    petClinicPhones?:IPhone[];
}