import { IFile } from "./IFile";
import { IPet } from "./IPet";

/** Needed in pet-edit-admin.component.ts - change it later there too for the IPet! */
export interface IPetWithUser{
    pet:IPet;
    username?:string;
}
