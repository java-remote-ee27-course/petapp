export interface IFile{
    id: number;
    imgName?:string;
    url:string;
    type?:string;
    size?:number;
    imgData?:string;
    date?:string;
}