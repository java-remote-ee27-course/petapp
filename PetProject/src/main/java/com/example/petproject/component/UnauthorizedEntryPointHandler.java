package com.example.petproject.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *  UnauthorizedEntryPointHandler implements the BasicAuthenticationEntryPoint interface.
 *  The interface commence() method is used to handle authentication errors,
 *  handles unauthorized access requests by setting the appropriate HTTP status code (401),
 *  sending WWW-Authenticate header,
 *  providing a JSON response with error details to the client,
 *  and creates a new HashMap object to store the error response data.
 *  uses the Jackson ObjectMapper to serialize the resp map into JSON format
 *  and sends it to output stream
 */

@Component
public class UnauthorizedEntryPointHandler implements AuthenticationEntryPoint {
    @Override
    public void commence(
            final HttpServletRequest request,
            final HttpServletResponse response,
            final AuthenticationException authException
    ) throws IOException {
        //sets the HTTP response status code to 401 Unauthorized
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        //sets the content type of the response to JSON -- providing meaningful error msg to client
        response.setContentType("application/json");
        //creates a new HashMap object to store the error response data
        Map<String, Object> resp = new HashMap<>();
        // adds an entry with the key "error" and
        // the value of the HTTP status code reason phrase (Unauthorized).
        resp.put("error", HttpStatus.UNAUTHORIZED.getReasonPhrase());
        //adds an entry to the resp map with the key "status"
        // and the value of the HTTP status code (401):
        resp.put("status", HttpStatus.UNAUTHORIZED.value());
        //provides more context about the error to the client:
        resp.put("message", authException.getMessage());
        //can be useful for debugging purposes:
        resp.put("timestamp", System.currentTimeMillis());
        // can be useful for identifying the resource
        // that the user was attempting to access:
        resp.put("path", request.getServletPath());
        //uses the Jackson ObjectMapper to serialize the resp map into JSON format
        // and write it to the response output stream. Sends the error response to client.
        new ObjectMapper().writeValue(response.getOutputStream(), resp);
    }

}

