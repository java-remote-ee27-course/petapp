package com.example.petproject.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ImageResponse {
        private String imgName;
        private String url;
        private String type;
        private String date;
        private long size;

        public ImageResponse(String imgName, String url, String type, long size) {
                this.imgName = imgName;
                this.url = url;
                this.type = type;
                this.size = size;
        }

}
