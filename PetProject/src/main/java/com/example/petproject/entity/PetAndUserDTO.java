package com.example.petproject.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PetAndUserDTO {
    private Pet pet;
    private String username;

    public PetAndUserDTO() {
        super();
    }

    public PetAndUserDTO(Pet pet, String username) {
        this.pet = pet;
        this.username = username;
    }
}
