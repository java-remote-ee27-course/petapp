package com.example.petproject.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import jakarta.persistence.*;
import lombok.*;
import org.springframework.lang.NonNull;

import java.time.LocalDateTime;
import java.util.Objects;

/**
 * Image data to be saved in DB (pets, clinics images etc.)
 * For reference see:
 * https://medium.com/shoutloudz/spring-boot-upload-and-download-images-using-jpa-b1c9ef174dc0
 * https://medium.com/@patelsajal2/how-to-create-a-spring-boot-rest-api-for-multipart-file-uploads-a-comprehensive-guide-b4d95ce3022b
 */

@Entity
@Getter
@Setter
@Builder
@ToString
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
public class ImageData {
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String type;
    private LocalDateTime localDateTime;

    @Lob
    @NonNull
    @Column(length=1000)
    private byte[] imageData;

    @ToString.Exclude
    @OneToOne(mappedBy = "petImage")
    @JsonBackReference(value="picture-of-pet")
    private Pet pet;

    public ImageData(String name, String type, @NonNull byte[] imageData) {
        this.name = name;
        this.type = type;
        this.imageData = imageData;
    }


    public ImageData(String name, String type, LocalDateTime localDateTime, @NonNull byte[] imageData) {
        this.name = name;
        this.type = type;
        this.localDateTime = localDateTime;
        this.imageData = imageData;
    }

    public ImageData(String name, String type, LocalDateTime localDateTime, @NonNull byte[] imageData, Pet pet) {
        this.name = name;
        this.type = type;
        this.localDateTime = localDateTime;
        this.imageData = imageData;
        this.pet = pet;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImageData imageData = (ImageData) o;
        return Objects.equals(id, imageData.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
