package com.example.petproject.entity;

import lombok.*;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class PetDTO {
    private Long id;
    private String name;
    private String petType;
    private ImageResponse picture;
    private Long userId;
    private String username;



    public PetDTO(Long id, String name, String petType) {
        this.id = id;
        this.name = name;
        this.petType = petType;
    }

    public PetDTO(String name, String petType) {
        this.name = name;
        this.petType = petType;
    }

    public PetDTO(String name, String petType, ImageResponse picture) {
        this.name = name;
        this.petType = petType;
        this.picture = picture;
    }

    public PetDTO(Long id, String name, String petType, ImageResponse picture) {
        this.id = id;
        this.name = name;
        this.petType = petType;
        this.picture = picture;
    }

    //22.02:
    public PetDTO(Long id, String name, String petType, ImageResponse picture, String username) {
        this.id = id;
        this.name = name;
        this.petType = petType;
        this.picture = picture;
        this.username = username;
    }

    //22.02:
    public PetDTO(Long id, String name, String petType, String username) {
        this.id = id;
        this.name = name;
        this.petType = petType;
        this.username = username;
    }
}
