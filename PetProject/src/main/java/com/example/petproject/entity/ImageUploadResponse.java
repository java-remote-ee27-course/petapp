package com.example.petproject.entity;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
public class ImageUploadResponse {
    private String responseMessage;
}
