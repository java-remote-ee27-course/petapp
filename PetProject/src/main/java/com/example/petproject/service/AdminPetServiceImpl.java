package com.example.petproject.service;


import com.example.petproject.entity.*;
import com.example.petproject.repository.ImageDataRepository;
import com.example.petproject.repository.PetRepository;
import com.example.petproject.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.openqa.selenium.By;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.List;
import java.util.Optional;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

/**
 * App admin actions to see pets, add, update and delete
 */
@Service("AdminPetService") //tells which PetService to use and when
@AllArgsConstructor
public class AdminPetServiceImpl implements AdminPetService<PetDTO>, PetService<Pet>{

    private PetRepository petRepository;
    private UserRepository userRepository;
    private PetServiceImpl petServiceImpl;
    private ImageDataRepository imageDataRepository;


    @Override
    public Pet getPet(Long id) {
        return petRepository.findById(id).orElseThrow(() ->
                new ResponseStatusException(HttpStatus.NOT_FOUND, "Pet not found"));
    }

    public List<Pet> getAllPets(){
        return petRepository.findAll();
    }

    @Override
    public List<PetDTO> getPetsWithTheirPicturesAndUserUsernames() {
        List<Pet> pets = getAllPets();
        //System.out.println("AdminPetServiceImpl: getAllPetsAndTheirPictures() Here we are");
        return pets.stream()
                .map(pet -> getAPetWithItsPictureAndUserUsername(pet))
                .toList();
    }

    public PetDTO getAPetWithItsPictureAndUserUsername(Pet pet){
        String fileDownloadUri;
        ImageData image = pet.getPetImage();
        PetDTO petDTO;

        //if image exists, return petDTO object with image,
        //if not, return petDTO object w/o image:
        if(image != null){
            fileDownloadUri = ServletUriComponentsBuilder
                    .fromCurrentContextPath()
                    .path("/files/images/")
                    .path(String.valueOf(image.getId()))
                    .toUriString();

            petDTO = new PetDTO(pet.getId(),
                                pet.getName(),
                                pet.getPetType(),
                                new ImageResponse(
                                    image.getName(),
                                    fileDownloadUri,
                                    image.getType(),
                                    image.getImageData().length),
                                pet.getUser().getUsername());
        } else {
            petDTO = new PetDTO(pet.getId(),
                                pet.getName(),
                                pet.getPetType(),
                                pet.getUser().getUsername());
        }
        return petDTO;
    }

    @Override
    public void addPet(Pet pet, Long imageId) {
        Pet newPet = new Pet();
        newPet.setName(pet.getName());
        newPet.setPetType(pet.getPetType());
        String username = pet.getUser().getUsername();
        User petOwner = userRepository.findByUsername(username).get();
        newPet.setUser(petOwner);
        if(imageId == 0) {
            petRepository.save(newPet);
        } else {
            ImageData image = imageDataRepository.findById(imageId).orElse(null);
            newPet.setPetImage(image);
            petRepository.save(newPet);
        }
    }


    @Override
    public void updatePet(Long id, Pet pet) {
        Optional<Pet> petToUpdate = petRepository.findById(id);
        User petUser = pet.getUser();
        petToUpdate
                .map(current -> {
                    current.setName(pet.getName());
                    current.setPetType(pet.getPetType());
                    if(!(petUser == null || petUser.getUsername().isBlank())){
                        current.setUser(petUser);
                    }
                    petRepository.save(current);
                    return new ResponseStatusException(HttpStatus.NO_CONTENT); //"Pet updated";
                })
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Pet not found"));
    }

    @Override
    public void deletePet(Long id) {
        if(petRepository.existsById(id)){
            petRepository.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Pet not found");
        }
    }


}
