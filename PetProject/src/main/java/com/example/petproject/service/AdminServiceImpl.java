package com.example.petproject.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

/**
 * App admin actions // to be updated
 */
@Service("AdminService") //tells which service to use and when
@AllArgsConstructor
public class AdminServiceImpl {

}
