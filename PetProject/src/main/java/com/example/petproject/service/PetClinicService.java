package com.example.petproject.service;

import com.example.petproject.entity.PetClinic;
import org.springframework.stereotype.Service;

import java.util.List;


public interface PetClinicService<T> {
    T getPetClinic(Long id);

    default PetClinic getClinic(Long id){
        return null;
    }

    List<T> getPetClinics();

}
