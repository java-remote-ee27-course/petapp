package com.example.petproject.service;

import com.example.petproject.entity.PetClinic;
import com.example.petproject.repository.PetClinicRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.Optional;

/**
 * App admin actions to add, update and delete clinics
 * TODO::implement them in frontend
 */

@Service("AdminPetClinic")
@AllArgsConstructor
public class AdminPetClinicServiceImpl implements AdminPetClinicService<PetClinic>{
    PetClinicRepository petClinicRepository;


    @Override
    public void updatePetClinic(Long id, PetClinic petClinic) {
        Optional<PetClinic> clinic = petClinicRepository.findById(id);
        clinic
                .map(current -> {
                    current.setPetClinicName(petClinic.getPetClinicName());
                    if ((petClinic.getUsers() != null)) {
                        current.setUsers(petClinic.getUsers());
                    } else {
                        current.setUsers(current.getUsers());
                    }
                    petClinicRepository.save(current);
                    return new ResponseStatusException(HttpStatus.NO_CONTENT);
                }).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Clinic not found"));
    }


    @Override
    public void addPetClinic(PetClinic petClinic) {
        petClinicRepository.save(petClinic);
    }


    @Override
    public void deletePetClinic(Long id) {
        if(petClinicRepository.existsById(id)){
            petClinicRepository.deleteById(id);
        } else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Clinic not found");
        }
    }
}
