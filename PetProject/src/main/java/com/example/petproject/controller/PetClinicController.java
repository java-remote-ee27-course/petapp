package com.example.petproject.controller;

import com.example.petproject.entity.Pet;
import com.example.petproject.entity.PetClinic;
import com.example.petproject.entity.PetClinicDTO;
import com.example.petproject.service.PetClinicService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * App Rest API controller to see clinics info
 *
 */

@RestController
@AllArgsConstructor
@RequestMapping("/api/petclinics")
public class PetClinicController {
    @Qualifier("PetClinic")
    PetClinicService<PetClinicDTO> petClinicService;

    @GetMapping({"", "/"})
    public List<PetClinicDTO> getAllPetClinics() {
        return petClinicService.getPetClinics();
    }

    @GetMapping("/{id}")
    public PetClinicDTO getPetClinic(@PathVariable Long id){
        return petClinicService.getPetClinic(id);
    }

}
