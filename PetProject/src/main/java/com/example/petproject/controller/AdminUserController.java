package com.example.petproject.controller;

import com.example.petproject.entity.User;
import com.example.petproject.entity.UserDTO;
import com.example.petproject.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

/**
 * App admin Rest API controller to see users, add, update and delete
 */

@RestController
@AllArgsConstructor
@RequestMapping("/api/admins")
@CrossOrigin("http://localhost:4200")           //delete it later, if not needed!
public class AdminUserController {

    @Qualifier("AdminUserService")
    UserService<User> userAdminService;


     @GetMapping("/users/{username}")
     public User user(@PathVariable String username) {
        return userAdminService.findUserByUsername(username).orElseThrow(() ->
            new ResponseStatusException(HttpStatus.NOT_FOUND, "User not found"));
     }

    @GetMapping("/users")
    public List<User> users() {
        return userAdminService.getUsers();
    }

    @GetMapping("/users/dto")
    public List<UserDTO> userDTOs() {
        return userAdminService.getUsersDTO();
    }
}
