package com.example.petproject.configuration;

import com.example.petproject.component.UnauthorizedEntryPointHandler;
import com.example.petproject.utility.RSAKeyProperties;
import com.nimbusds.jose.jwk.JWK;
import com.nimbusds.jose.jwk.JWKSet;
import com.nimbusds.jose.jwk.RSAKey;
import com.nimbusds.jose.jwk.source.ImmutableJWKSet;
import com.nimbusds.jose.jwk.source.JWKSource;
import com.nimbusds.jose.proc.SecurityContext;
import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.JwtEncoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtEncoder;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.security.web.SecurityFilterChain;

import static jakarta.servlet.DispatcherType.ERROR;
import static jakarta.servlet.DispatcherType.FORWARD;


@Configuration
@EnableWebSecurity
@AllArgsConstructor
//@EnableMethodSecurity(prePostEnabled = false)
public class SecurityConfiguration {

    private final RSAKeyProperties keys;


     //AuthenticationConfiguration class:
    // This class encapsulates the authentication configuration logic,
    // including defining the userDetailsService() and passwordEncoder() methods.
    // getAuthenticationManager(): returns the configured AuthenticationManager bean,
    // which is responsible for handling user authentication
    @Bean
    public AuthenticationManager authManager(UserDetailsService userDetailsService) {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(userDetailsService);
        daoAuthenticationProvider.setPasswordEncoder(passwordEncoder());
        return new ProviderManager(daoAuthenticationProvider);
    }


    @Bean
    public PasswordEncoder passwordEncoder(){
        return new BCryptPasswordEncoder();
    }


    //csrf is disabled due to using REST API / angular
    //SecurityFilterChain is responsible for configuring the security rules for incoming HTTP requests.
    // http.sessionManagement() defines how HTTP sessions are handled for authentication purposes.
    // STATELESS-- sessions are not created for each request, improving security and efficiency.
    // exceptionHandling - handles unauthorized requests.

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
                .sessionManagement((session) -> session
                        .sessionCreationPolicy(SessionCreationPolicy.STATELESS))
                .authorizeHttpRequests(reqs -> reqs
                        .dispatcherTypeMatchers(FORWARD, ERROR).permitAll()
                        .requestMatchers("/api/petclinics", "/api/petclinics/*").permitAll()
                        .requestMatchers("/files", "/files/**").permitAll()             //TODO:: change it later, for test only!
                        .requestMatchers("/api/auth/**").permitAll()
                        .requestMatchers("/api/admins/**").hasRole("ADMIN")
                        .requestMatchers("/api/users/**").hasAnyRole("ADMIN", "USER")
                        .anyRequest().authenticated())
                .oauth2ResourceServer((oauth2) -> oauth2
                        .jwt((jwt) ->
                                jwt.jwtAuthenticationConverter(jwtAuthenticationConverter())))
                .exceptionHandling((exception) -> exception
                        .authenticationEntryPoint(new UnauthorizedEntryPointHandler()));

        return http.build();
    }


    //https://docs.spring.io/spring-security/reference/5.8/migration/servlet/config.html
    // Ignore this pages of authentication list, do not ask any auth:
    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        return (web) -> web.ignoring().requestMatchers("/api/auth/register", "/api/auth/login", "/files", "/files/**");  //TODO::change files access rights later
    }

    //Gets data out of token
    @Bean
    public JwtDecoder jwtDecoder(){
        System.out.println("In JwtDecoder, public key" + keys.getPublicKey());
        return NimbusJwtDecoder.withPublicKey(keys.getPublicKey()).build();
    }

    //Bundles up w some data and signs with public and private key and spits it out for us to use
    @Bean
    public JwtEncoder jwtEncoder(){
        JWK jwk = new RSAKey.Builder(keys.getPublicKey()).privateKey(keys.getPrivateKey()).build();

        System.out.println("In JwtEncoder, public key" + keys.getPublicKey());
        System.out.println("In JwtEncoder, private key" + keys.getPrivateKey());
        JWKSource<SecurityContext> jwks = new ImmutableJWKSet<>(new JWKSet(jwk));
        return new NimbusJwtEncoder(jwks);
    }

    // Spring security expects to have roles with prefix ROLE_
    // Next nmethod converts role "user", "admin" to "ROLE_USER", "ROLE_ADMIN"
    // (we dont want role_user, role_admin in our sql table,
    // so that is why we initially named them role, admin
    // and now convert all roles inside roles claim to ROLE_ ...
    @Bean
    public JwtAuthenticationConverter jwtAuthenticationConverter(){
        JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName("roles");
        jwtGrantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
        JwtAuthenticationConverter jwtConverter= new JwtAuthenticationConverter();
        jwtConverter.setJwtGrantedAuthoritiesConverter(jwtGrantedAuthoritiesConverter);
        System.out.println("jwtConverter "+ jwtConverter);
        return jwtConverter;
    }
}
