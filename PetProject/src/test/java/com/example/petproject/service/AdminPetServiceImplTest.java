package com.example.petproject.service;

import com.example.petproject.entity.*;
import com.example.petproject.repository.PetRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.mockito.Mockito.*;

//Mockito Junit tests and Selenide e2e tests
@ExtendWith(MockitoExtension.class)
public class AdminPetServiceImplTest {
    private static final Long ID = 1L;
    private static final User USER = new User("user", "pass");
    private static final ImageData IMG_DATA = new ImageData("Petpicture", "image/png", new byte[25]);
    private static final Pet PET = new Pet(ID, "Pauka", "Dog", USER, IMG_DATA);
    private static final List<Pet> PETS = new ArrayList<>(List.of(PET));



    @Mock
    private PetRepository petRepository;

    @InjectMocks
    private AdminPetServiceImpl adminPetServiceImpl;

    @Test
    void shouldGetPetById() {
        //given
        when(petRepository.findById(ID)).thenReturn(Optional.of(PET));
        //when
        final Pet actualPet = adminPetServiceImpl.getPet(ID);
        //then
        assertThat(actualPet).isEqualTo(PET);
    }

    @Test
    void shouldThrowResponseStatusException_WhenPetEntryIsNotFoundById(){
        //given
        when(petRepository.findById(ID)).thenReturn(Optional.empty());

        //then, when
        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> adminPetServiceImpl.getPet(ID));
    }

    @Test
    void shouldThrowResponseStatusException_WithMessage404_WhenPetEntryIsNotFoundById(){
        //given
        when(petRepository.findById(ID)).thenThrow(new ResponseStatusException(HttpStatus.NOT_FOUND, "Pet not found"));

        //then, when
        assertThatExceptionOfType(ResponseStatusException.class)
                .isThrownBy(() -> adminPetServiceImpl.getPet(ID))
                .withMessage("404 NOT_FOUND \"Pet not found\"");

    }

    @Test
    void shouldGetAllPetsFromPetRepository_WhenThereAreTwoPetsInTheRepository(){
        //given
        Pet PET1 = new Pet(1L, "Pauka", "Dog", USER, IMG_DATA);
        Pet PET2 = new Pet(2L, "Murka", "Dog", USER, IMG_DATA);
        final List<Pet> PETS = new ArrayList<>();
        PETS.add(PET1);
        PETS.add(PET2);
        when(petRepository.findAll()).thenReturn(PETS);

        //when
        List<Pet> actualPets = adminPetServiceImpl.getAllPets();

        //then
        assertThat(actualPets).isEqualTo(PETS);
    }


}
