package com.example.petproject.service;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Configuration.baseUrl;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

//E2E tests are using Selenide

class AuthenticationServiceTest {

    private String baseUrl = "http://localhost:4200";

    @Test
    public void userCanLoginByUsername() {
        //given
        open(baseUrl + "/login?returnUrl=%2Fpets");
        $(By.name("username")).setValue("admin1");
        $(By.name("password")).setValue("admin");
        //when
        $(".btn").shouldBe(visible);
        $(".btn").click();
        //then
        $(".loading_progress").should(disappear); // Waits until element disappears
        $("h1").shouldHave(text("Your pets")); // Waits until element gets text
    }

    @Test
    public void userCanLogout() {
        //given
        open(baseUrl + "/login?returnUrl=%2Fpets");
        $(By.name("username")).setValue("admin1");
        $(By.name("password")).setValue("admin");
        //when
        $(".btn").shouldBe(visible);
        $(".btn").click();
        $(".loading_progress").should(disappear);
        $(".navbar-nav").shouldBe(visible);
        //then
        $(byText("Logout")).click();
        $("h1").shouldHave(text("Login"));
    }
}