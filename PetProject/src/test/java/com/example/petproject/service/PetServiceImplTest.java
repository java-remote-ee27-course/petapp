package com.example.petproject.service;

import com.codeborne.selenide.WebDriverRunner;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PetServiceImplTest {

    private final String baseUrl = "http://localhost:4200";

    @BeforeEach
    void changeChromeSettingsOfPasswordManagerLeakDetectionFalseForTesting(){
        //for selenide tests; see:
        //https://github.com/selenide/selenide/discussions/2658
        System.setProperty("chromeoptions.prefs", "profile.password_manager_leak_detection=false");
    }

    //e2e selenide tests:
    @Test
    void adminCanLoginWithAdminRights_AndAddAPetWithPetsNameAndType_AsARegularUser() {
        //given

        open(baseUrl + "/login?returnUrl=%2Fpets");
        $(By.name("username")).setValue("admin1");
        $(By.name("password")).setValue("admin");

        //when
        $(".btn").shouldBe(visible);
        $(".btn").click();
        $(".loading_progress").should(disappear);

        $(By.name("name")).setValue("TippyTheDog");
        $(By.name("petType")).setValue("Dog");
        $(".form-control").shouldBe(visible);
        $(byText("Add pet")).click();
        $(".loading_progress").should(disappear);
        $(".btn").shouldBe(visible);

        //then
        $(byText("TippyTheDog")).shouldBe(visible);
    }



    //Prerequisites: No pets should be created before running the test
    @Test
    void adminCanLoginWithAdminRights_AddAPetAndDeleteAPet_AsARegularUser(){
        int size = 0;
        int newSize = 0;
        List<WebElement> deleteButtons;

        open(baseUrl + "/login?returnUrl=%2Fpets");
        $(".loading_progress").should(disappear);

        $(By.name("username")).setValue("admin1");
        $(By.name("password")).setValue("admin");

        $(".btn").shouldBe(visible);
        $(".btn").click();

        $(By.name("name")).shouldBe(visible);

        //size of delete buttons list before adding an entry
        deleteButtons = WebDriverRunner.getWebDriver().findElements(byText("Delete"));
        size = deleteButtons.size();

        //create a pet just in case if there are no pets in the table and in order not to delete an existing pet:
        $(By.name("name")).setValue("TippyTheDog");
        $(By.name("petType")).setValue("Dog");
        $(byText("Add pet")).shouldBe(visible);
        $(byText("Add pet")).click();

        $(".btn-danger").shouldBe(visible);
        $(".btn-danger").click();

        $(byText("Add pet")).shouldBe(visible);

        //size of delete buttons list after deletion an entry
        deleteButtons = WebDriverRunner.getWebDriver().findElements(byText("Delete"));
        newSize = deleteButtons.size();

        assertThat(newSize).isEqualTo(size);
    }

}